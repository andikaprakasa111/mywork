import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class CodeExample {

    public static void main(String[] args) throws IOException {
        System.out.print("Input the number of families :");
        Scanner myObj = new Scanner(System.in);
        Integer input = myObj.nextInt();
        BufferedReader bi = new BufferedReader(new InputStreamReader(System.in));
        int[] num = new int[input];
        int bus = 0;
        System.out.print("Input the number of members in the family (separated by a space) :");
        String[] strNums = bi.readLine().split(" ");
        if (strNums.length <= input && strNums.length >= input) {
            int i;
            for (i = 0; i < strNums.length; ++i) {
                num[i] = Integer.parseInt(strNums[i]);
            }

            for (i = 0; i < strNums.length; ++i) {
                label104:
                for (i = 0; i < strNums.length; ++i) {
                    if (i + 1 == strNums.length && num[i] < 4 && num[i] != 0) {
                        ++bus;
                    } else if (num[i] != 0) {
                        if (num[i] == 4) {
                            ++bus;
                        } else if (num[i] >= 4) {
                            if (num[i] > 4) {
                                int sisa = num[i] % 4;
                                bus += Math.floorDiv(num[i], 4);
                                num = add_element(i + 1, num, sisa);
                                strNums = new String[strNums.length + 1];
                            }
                        } else {
                            for (int j = i + 1; j < strNums.length; ++j) {
                                int k;
                                for (k = j; k < strNums.length; ++k) {
                                    if (num[i] + num[k] == 4 && num[k] != 0) {
                                        num[i] += num[k];
                                        num[k] = 0;
                                        ++bus;
                                        continue label104;
                                    }
                                }

                                for (k = j; k < strNums.length; ++k) {
                                    if (num[i] + num[k] == 3 && num[k] != 0) {
                                        num[i] += num[k];
                                        num[k] = 0;
                                        ++bus;
                                        continue label104;
                                    }
                                }

                                for (k = j; k < strNums.length; ++k) {
                                    if (num[i] + num[k] == 2 && num[k] != 0) {
                                        num[i] += num[k];
                                        num[k] = 0;
                                        ++bus;
                                        continue label104;
                                    }
                                }

                                if (isValidIndex(num, i + 1)) {
                                    num[i + 1] += num[i];
                                    num[i] = 0;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            System.out.print("Minimum bus required is : " + bus);
        } else {
            System.out.print("Input must be equal with count of family");
        }

    }

    public static int[] add_element(int n, int[] myarray, int ele) {
        int[] dest_Array = new int[myarray.length + 1];
        int j = 0;

        for (int i = 0; i < dest_Array.length; ++i) {
            if (i == n) {
                dest_Array[i] = ele;
            } else {
                dest_Array[i] = myarray[j];
                ++j;
            }
        }

        return dest_Array;
    }

    public static boolean isValidIndex(int[] arr, int index) {
        return index >= 0 && index < arr.length;
    }

}
